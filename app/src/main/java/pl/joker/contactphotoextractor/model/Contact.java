package pl.joker.contactphotoextractor.model;


import android.net.Uri;

import lombok.Builder;
import lombok.Getter;


@Builder
@Getter
public class Contact {
    private String id;
    private String name;
    private Uri photoUri;
}
