package pl.joker.contactphotoextractor;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;
import pl.joker.contactphotoextractor.services.BitmapCreator;
import pl.joker.contactphotoextractor.services.SavePhoto;

public class ContactPhotoDetails extends AppCompatActivity implements DialogInterface.OnClickListener {

    private final String SAVE_DIALOG = "save_dialog";

    @BindView(R.id.photo_full_view)
    protected ImageView imageView;

    private String contactName;
    private String contactPhotoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_photo_details);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        contactName = extras.getString(MainActivity.CONTACT_NAME);
        setActionBarTitle(contactName);
        contactPhotoUri = extras.getString(MainActivity.CONTACT_PHOTO_URI);
        setImageViewPhoto(contactPhotoUri);
    }

    @OnClick(R.id.fab_save_button)
    protected void showSaveDialog() {
        SaveDialog saveDialog = new SaveDialog();
        saveDialog.setListener(this);
        saveDialog.show(getSupportFragmentManager(), SAVE_DIALOG);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        BitmapCreator bitmapCreator = new BitmapCreator(this);
        try {
            Bitmap contactPhoto = bitmapCreator.createBitmap(contactPhotoUri);
            SavePhoto savePhoto = new SavePhoto();
            savePhoto.saveBitmapInExternalStorage(contactName, contactPhoto);
            Toast.makeText(this, "Photo was saved in folder CONTACTS_PHOTOS", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            Toast.makeText(this, "Something went wrong while saving...", Toast.LENGTH_SHORT).show();
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    private void setActionBarTitle(String contactName) {
        getSupportActionBar().setTitle(contactName);
    }

    private void setImageViewPhoto(String contactPhotoUri) {
        imageView.setImageURI(Uri.parse(contactPhotoUri));
    }
}
