package pl.joker.contactphotoextractor.services;


import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;

public class BitmapCreator {

    private Context mContext;

    public BitmapCreator(Context context) {
        mContext = context;
    }

    public Bitmap createBitmap(String photoUri) throws IOException {
        Uri uri = Uri.parse(photoUri);
        AssetFileDescriptor assetFileDescriptor =
                mContext.getContentResolver().openAssetFileDescriptor(uri, "r");
        InputStream inputStream = assetFileDescriptor.createInputStream();
        return BitmapFactory.decodeStream(inputStream);
    }
}
