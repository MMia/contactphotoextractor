package pl.joker.contactphotoextractor.services;


import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class SavePhoto {

    private static final String SUFFIX = ".jpeg";
    private static final String FOLDER_NAME = "/CONTACTS_PHOTOS";
    private static final String ABSOLUTE_PATH =
            Environment.getExternalStorageDirectory().getAbsolutePath();

    public void saveBitmapInExternalStorage(String contactName, Bitmap photoBitmap) throws IOException {
        String folderPath = ABSOLUTE_PATH + FOLDER_NAME;
        checkAndCreateFolder(folderPath);
        photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100,
                new FileOutputStream(folderPath + "/" + contactName + SUFFIX));
    }

    private void checkAndCreateFolder(String path) {
        File folderFile = new File(path);
        if (!folderFile.exists()) {
            folderFile.mkdir();
        }
    }
}
