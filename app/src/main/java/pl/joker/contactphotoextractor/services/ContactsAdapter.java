package pl.joker.contactphotoextractor.services;


import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.joker.contactphotoextractor.R;
import pl.joker.contactphotoextractor.model.Contact;


public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactViewHolder> {

    private List<Contact> mContacts = Collections.emptyList();
    private RecyclerViewItemClickedListener mItemListener;

    public ContactsAdapter(RecyclerViewItemClickedListener pItemListener) {
        mItemListener = pItemListener;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_holder, parent, false);
        return new ContactsAdapter.ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        final Contact contact = mContacts.get(position);
        String name = contact.getName();
        holder.contactName.setText(name);
        Context context = holder.contactName.getContext();
        setFont(context, holder.contactName);
        Uri photoUri = contact.getPhotoUri();
        holder.imageView.setImageURI(photoUri);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemListener.recyclerViewItemClicked(contact);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    public void setData(List<Contact> data) {
        mContacts = data;
        notifyDataSetChanged();
    }

    private void setFont(Context context, TextView textView) {
        AssetManager assetManager = context.getApplicationContext().getAssets();
        Typeface typeface = Typeface.createFromAsset(assetManager, String.format(Locale.getDefault(), "fonts/%s", "Roboto-Light.ttf"));
        textView.setTypeface(typeface);
    }

    protected static class ContactViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_view)
        ImageView imageView;

        @BindView(R.id.contact_name)
        TextView contactName;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
