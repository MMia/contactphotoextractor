package pl.joker.contactphotoextractor.services;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import pl.joker.contactphotoextractor.model.Contact;

@Getter
public class ContactsProvider extends AsyncTask<Void, Void, List<Contact>> {
    private Context mContext;
    private ResultListener mListener;

    public ContactsProvider(Context context, ResultListener pListener) {
        mContext = context;
        mListener = pListener;
    }

    @Override
    protected List<Contact> doInBackground(Void... params) {
        return extractContactsData();
    }

    @Override
    protected void onPostExecute(List<Contact> contacts) {
        mListener.onResultSucceed(contacts);
    }

    private List<Contact> extractContactsData() {
        List<Contact> contacts = new ArrayList<>();
        Cursor cursor = getCursor();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                if (entryHasPhoto(cursor)) {
                    Contact contact = buildContactData(cursor);
                    contacts.add(contact);
                }
            }
        }
        cursor.close();

        return contacts;
    }

    private Cursor getCursor() {
        ContentResolver contentResolver = mContext.getContentResolver();
        return contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
    }

    private boolean entryHasPhoto(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_FILE_ID)) != null;
    }

    @Nullable
    private Contact buildContactData(Cursor cursor) {
        String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(id));
        Uri photoUri = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);

        return Contact.builder()
                .id(id)
                .name(name)
                .photoUri(photoUri)
                .build();
    }
}
