package pl.joker.contactphotoextractor.services;


import pl.joker.contactphotoextractor.model.Contact;

public interface RecyclerViewItemClickedListener {
    void recyclerViewItemClicked(Contact contact);
}
