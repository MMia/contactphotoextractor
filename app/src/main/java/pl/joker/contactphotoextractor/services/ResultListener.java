package pl.joker.contactphotoextractor.services;


import java.util.List;

import pl.joker.contactphotoextractor.model.Contact;

public interface ResultListener {
    void onResultSucceed(List<Contact> result);
}
