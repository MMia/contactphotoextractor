package pl.joker.contactphotoextractor;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.joker.contactphotoextractor.model.Contact;
import pl.joker.contactphotoextractor.services.ContactsAdapter;
import pl.joker.contactphotoextractor.services.ContactsProvider;
import pl.joker.contactphotoextractor.services.RecyclerViewItemClickedListener;
import pl.joker.contactphotoextractor.services.ResultListener;

public class MainActivity extends AppCompatActivity implements ResultListener, RecyclerViewItemClickedListener {

    public static final String CONTACT_NAME = "CONTACT_NAME";
    public static final String CONTACT_PHOTO_URI = "CONTACT_PHOTO_URI";

    private final int spanCount = 2;

    private boolean permissionsChecked = false;

    @BindView(R.id.contacts_list)
    protected RecyclerView recyclerView;

    private ContactsProvider contactsProvider;
    private ContactsAdapter contactsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));

        contactsProvider = new ContactsProvider(this.getApplicationContext(), this);
        contactsProvider.execute();

        contactsAdapter = new ContactsAdapter(this);
        recyclerView.setAdapter(contactsAdapter);

        isStoragePermissionGranted();
    }

    @Override
    public void onResultSucceed(List<Contact> result) {
        contactsAdapter.setData(result);
    }

    @Override
    public void recyclerViewItemClicked(Contact contact) {
        final String contactName = contact.getName();
        final String contactPhotoUri = contact.getPhotoUri().toString();
        Intent intent = new Intent(this, ContactPhotoDetails.class);
        Bundle extras = new Bundle();
        extras.putString(CONTACT_NAME, contactName);
        extras.putString(CONTACT_PHOTO_URI, contactPhotoUri);
        intent.putExtras(extras);
        startActivity(intent);
    }

    public void isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                permissionsChecked = true;
                Log.d(getClass().getName(), "Permission already granted");
            } else {
                Log.d(getClass().getName(), "Requesting permission");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        } else {
            Log.d(getClass().getName(), "Permission already granted");
        }
    }
}
